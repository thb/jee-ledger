CREATE TABLE Entry (
	id BIGINT NOT NULL,
	date DATETIME,
	payer VARCHAR(255),
	title VARCHAR(255),
	value DOUBLE NOT NULL,
	pageId BIGINT,
	PRIMARY KEY (id)
) ENGINE=MyISAM;

CREATE TABLE Page (
	id BIGINT NOT NULL,
	title VARCHAR(255),
	PRIMARY KEY (id)
) ENGINE=MyISAM;

CREATE TABLE hibernate_sequence (
	next_val BIGINT
) ENGINE=InnoDB;

CREATE INDEX FKd5j4omber2huen1t6pedc4nvg ON Entry (pageId ASC);
CREATE INDEX date_desc ON Entry (date DESC);
CREATE INDEX date_asc ON Entry (date ASC);

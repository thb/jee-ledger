INSERT INTO `Page` (`id`, `title`) VALUES
(0, 'Title');

INSERT INTO `Entry` (`id`, `date`, `payer`, `title`, `value`, `pageId`) VALUES
(1, '2018-05-04 00:00:00', 'Max Mustermann', 'Entry 1', 20, 0),
(3, '2018-06-12 00:00:00', 'Max Mustermann', 'Entry 2', -4, 0),
(2, '2018-06-13 00:00:00', 'Maxi Musterfrau', 'Entry 3', -5, 0);

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(4);

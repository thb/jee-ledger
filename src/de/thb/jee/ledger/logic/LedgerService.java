package de.thb.jee.ledger.logic;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import de.thb.jee.ledger.models.Entry;
import de.thb.jee.ledger.models.Page;
import de.thb.jee.ledger.models.repository.api.EntryRepository;
import de.thb.jee.ledger.models.repository.api.PageRepository;


@Transactional
public class LedgerService {

  @Inject
  private PageRepository mPageRepo;
  @Inject
  private EntryRepository mEntryRepo;

  private static final DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");

  public List<Page> getAllPages() {
    return mPageRepo.readAll();
  }

  public void createEntry(Entry entry) {
    if (entry.getId() == null) {
      mEntryRepo.create(entry);
    } else {
      mEntryRepo.update(entry);
    }
  }

  public void deleteEntry(Entry entry) {
    mEntryRepo.delete(entry);
  }

  public List<Entry> getEntriesOfMonthRange(Long pageId, YearMonth startMonth, YearMonth endMonth) {
    return mEntryRepo.getEntriesOfMonthRange(pageId, startMonth, endMonth);
  }

  public List<Entry> getEntriesReverseDate(Long pageId) {
    return mEntryRepo.getEntries(pageId);
  }

  public Double getSaldo(Long pageId, LocalDateTime date, Long entryId) {
    return mEntryRepo.getSaldo(pageId, date, entryId);
  }

  public Double getDateSaldo(Long pageId, LocalDateTime date) {
    return mEntryRepo.getSaldo(pageId, date, null);
  }

  public List<Entry> getFilteredEntries(Long pageId, String title, String payer, Double value, String comparator, LocalDateTime beginDate, LocalDateTime endDate) {
    return mEntryRepo.getFilteredEntries(pageId, title, payer, value, comparator, beginDate, endDate);
  }

  public LocalDateTime getDateFromFormattedString(String dateString) {
    try {
      LocalDate date = LocalDate.parse(dateString, LedgerService.DEFAULT_FORMATTER);
      LocalDateTime dateTime = LocalDateTime.of(date, LocalTime.MIN);
      return dateTime;
    } catch (DateTimeParseException e) {
      return null;
    }
  }

  public String getFormattedDateString(LocalDateTime date) {
    return date.format(LedgerService.DEFAULT_FORMATTER);
  }
}

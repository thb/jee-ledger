package de.thb.jee.ledger.ui;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.thb.jee.ledger.logic.LedgerService;
import de.thb.jee.ledger.models.Entry;
import de.thb.jee.ledger.models.Page;

@Named
@RequestScoped
public class PageController {

  @Inject
  private LedgerService ledgerService;
  @Inject
  private PageState pageState;
  @Inject
  private FilterController filterController;
  private int show;
  private static final int DEFAULT_STEP = 10;

  public PageController() {
    this.show = 10;
  }

  public String showMore (int oldShow) {
    this.show = oldShow + DEFAULT_STEP;
    return null;
  }

  public List<Entry> getEntriesReverse() {
    return this.ledgerService.getEntriesReverseDate(this.getPage().getId());
  }

  public Page getPage() {
    return this.pageState.getPage();
  }

  @PostConstruct
  public void setPage() {
    if (this.pageState.getPage() == null) {
      this.pageState.setPage(this.ledgerService.getAllPages().get(0));
    }
  }

  public List<Entry> getEntries() {
    if (this.pageState.getEntries() == null) {
      this.pageState.setEntries(this.getEntriesReverse());
    }
    return this.pageState.getEntries();
  }

  public Entry getPreviousEntry(int index) {
    Entry result = null;
    if(index > 0) {
    result = this.getEntries().get(index - 1);
      return result;
    }
    if(result == null) {
      result = this.getEntries().get(index);
    }
    return result;
  }

  public int getShow() {
    return show;
  }

  public void setShow(int show) {
    this.show = show;
  }

  public String summary() {
    return this.summary(null);
  }

  public String summary(LocalDateTime date) {
    if (date != null) {
      LocalDateTime start = date.with(TemporalAdjusters.firstDayOfMonth());
      start = start.with(LocalTime.MIN);
      LocalDateTime end = date.with(TemporalAdjusters.lastDayOfMonth());
      end = end.with(LocalTime.MAX);
      this.filterController.setBeginDate(start);
      this.filterController.setEndDate(end);
      this.filterController.filterEntries();
    }
    return "summary";
  }
}

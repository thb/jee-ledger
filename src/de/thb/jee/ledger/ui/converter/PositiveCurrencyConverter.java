package de.thb.jee.ledger.ui.converter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

// based on javax.faces.convert.NumberConverter
@FacesConverter("de.thb.jee.ledger.beans.converter.PositiveCurrencyConverter")
public class PositiveCurrencyConverter implements Converter {

  private String symbol = "";

  //Always parses positive value
  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {
    if (context == null || component == null) {
      throw new NullPointerException();
    }

    try {
      if (value == null) {
        return (null);
      }
      value = value.trim();
      if (value.length() < 1) {
        return (null);
      }

      NumberFormat parser = this.getFormatter();

      //pseudo negative prefix to allow parsing of value if there is no negative prefix or suffix (also parse a positive value)
      DecimalFormat df = (DecimalFormat) parser;
      df.setNegativePrefix("-");

      return parser.parse(value);
    } catch (Exception e) {
      throw new ConverterException(e);
    }
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (context == null || component == null) {
      throw new NullPointerException();
    }

    try {
      if (value == null) {
        return "";
      }

      if (value instanceof String) {
        return (String) value;
      }

      NumberFormat formatter = this.getFormatter();

      return (formatter.format(value));
    } catch (Exception e) {
      throw new ConverterException(e);
    }
  }

  private NumberFormat getFormatter() {
    NumberFormat formatter = NumberFormat.getCurrencyInstance();

    DecimalFormat df = (DecimalFormat) formatter;
    DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
    dfs.setCurrencySymbol(this.symbol);
    df.setDecimalFormatSymbols(dfs);
    df.setNegativePrefix(this.symbol);
    df.setNegativeSuffix("");

    return formatter;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }
}

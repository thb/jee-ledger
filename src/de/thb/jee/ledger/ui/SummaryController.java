package de.thb.jee.ledger.ui;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.thb.jee.ledger.logic.LedgerService;
import de.thb.jee.ledger.models.Entry;
import de.thb.jee.ledger.ui.models.BilancePieChartData;
import de.thb.jee.ledger.ui.models.LineChartData;

@Named
@RequestScoped
public class SummaryController {
  private List<Entry> entries;
  private YearMonth startMonth;
  private YearMonth endMonth;
  private List<LineChartData> totalGraphData;
  private List<LineChartData> monthGraphData;
  private BilancePieChartData pieData;
  private boolean dirty;

  @Inject
  LedgerService ledgerService;
  @Inject
  PageState pageState;

  @PostConstruct
  public void init() {
    this.dirty = false;
    this.prepareBilancePie();
    this.prepareBilanceGraphs();
  }

  @Deprecated //in favor of using the filters (might get reimplemented)
  public void readParams() {
    Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
    YearMonth now = YearMonth.now();
    String month = requestParams.getOrDefault("month", now.format(formatter));
    String startMonth = requestParams.get("startMonth");
    String endMonth = requestParams.get("endMonth");
    if(startMonth == null || endMonth == null) {
      startMonth = month;
      endMonth = month;
    }
    this.startMonth = YearMonth.parse(startMonth, formatter);
    this.endMonth = YearMonth.parse(endMonth, formatter);
  }

  public String getStartMonth() {
    return this.startMonth.getMonth().name();
  }

  public int getStartYear() {
    return this.startMonth.getYear();
  }

  public String getEndMonth() {
    return this.endMonth.getMonth().name();
  }

  public int getEndYear() {
    return this.endMonth.getYear();
  }

  public String getTotalGraphData() {
    return this.getGraphData(this.totalGraphData);
  }

  public String getMonthGraphData() {
    return this.getGraphData(this.monthGraphData);
  }

  public String getPieData() {
    return this.pieData.toJSONObject();
  }

  private void prepareBilancePie() {
    List<Entry> entries = this.getEntries();
    this.pieData = new BilancePieChartData();
    double incomes = 0f;
    double expenses = 0f;
    for(Entry entry : entries) {
      if (entry.getValue() > 0) {
        incomes += entry.getValue();
      } else {
        expenses += Math.abs(entry.getValue());
      }
    }
    this.pieData.setIncomes(incomes);
    this.pieData.setExpenses(expenses);
  }

  private void prepareBilanceGraphs() {
    LocalDateTime lastDateTime = null;
    int monthSaldo = 0;
    List<Entry> entries = this.getEntries();
    this.totalGraphData = new ArrayList<LineChartData>();
    this.monthGraphData = new ArrayList<LineChartData>();
    for(Entry entry : entries) {
      if (lastDateTime != null) {
        LocalDate lastDate = lastDateTime.toLocalDate();
        if (entry.getDate().toLocalDate().isAfter(lastDate)) {
          this.totalGraphData.add(new LineChartData(lastDate, this.ledgerService.getDateSaldo(this.pageState.getPage().getId(), lastDateTime)));
          this.monthGraphData.add(new LineChartData(lastDate, monthSaldo));
        }
      }
      lastDateTime = entry.getDate();
      monthSaldo += entry.getValue();
    }
    if (lastDateTime != null) {
      this.totalGraphData.add(new LineChartData(lastDateTime.toLocalDate(), this.ledgerService.getDateSaldo(this.pageState.getPage().getId(), lastDateTime)));
      this.monthGraphData.add(new LineChartData(lastDateTime.toLocalDate(), monthSaldo));
    }
  }

  private List<Entry> getEntries() {
    if(this.entries == null) {
      ArrayList<Entry> entries = new ArrayList<Entry>(this.pageState.getEntries());
      Collections.reverse(entries);
      this.entries = entries;
    }
    return this.entries;
  }

  private String getGraphData(List<LineChartData> data) {
    String dataString = "";
    for (LineChartData chartData : data) {
      dataString = dataString + chartData.toJSONObject() + ",";
    }
    return dataString;
  }

  public boolean isDirty() {
    return this.dirty;
  }

  public void setDirty(boolean dirty) {
    this.dirty = dirty;
  }
}

package de.thb.jee.ledger.ui.models;

import java.time.LocalDate;
import java.time.ZoneId;

public class LineChartData {
  private LocalDate x;
  private double y;

  public LineChartData(LocalDate x, double y) {
    this.x = x;
    this.y = y;
  }

  public LocalDate getX() {
    return x;
  }

  public void setX(LocalDate x) {
    this.x = x;
  }

  public double getY() {
    return y;
  }

  public void setY(double y) {
    this.y = y;
  }

  public String toJSONObject() {
    ZoneId zoneId = ZoneId.systemDefault();
    long epoch = x.atStartOfDay(zoneId).toEpochSecond()*1000;
    return String.format("{x: %d, y: %f}",epoch,y);
  }
}

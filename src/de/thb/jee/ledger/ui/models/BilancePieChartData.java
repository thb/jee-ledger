package de.thb.jee.ledger.ui.models;

import de.thb.jee.ledger.utilities.MathUtils;

public class BilancePieChartData {
  private double incomes;
  private double expenses;

  public double getIncomes() {
    return incomes;
  }

  public void setIncomes(double incomes) {
    this.incomes = incomes;
  }

  public double getExpenses() {
    return expenses;
  }

  public void setExpenses(double expenses) {
    this.expenses = expenses;
  }

  public String toJSONObject() {
    double total = incomes + expenses;
    double incomePercentage = MathUtils.getPercentage(incomes, total, 2);
    double expensePercentage = MathUtils.getPercentage(expenses, total, 2);
    return String.format("{labels: [%f, %f], values: [%f, %f]}", incomes, expenses, incomePercentage, expensePercentage);
  }
}

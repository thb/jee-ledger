package de.thb.jee.ledger.ui;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class SettingsController implements Serializable {
  private static final long serialVersionUID = -1177770126323670615L;
  private boolean darkmode;

  public boolean isDarkmode() {
    return darkmode;
  }

  public void setDarkmode(boolean darkmode) {
    this.darkmode = darkmode;
  }

  public String save() {
    return null;
  }
}

package de.thb.jee.ledger.ui;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.thb.jee.ledger.logic.LedgerService;
import de.thb.jee.ledger.models.Entry;

@Named
@SessionScoped
public class FilterController implements Serializable {

  private static final long serialVersionUID = 8102405335757059643L;
  private String title;
  private double threshold;
  private String payer;
  private String comparator;
  private LocalDateTime beginDate;
  private LocalDateTime endDate;

  @Inject
  private PageState pageState;
  @Inject
  private transient LedgerService ledgerService;
  @Inject
  private SummaryController summaryController;

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getComparator() {
    return comparator;
  }

  public void setComparator(String comparator) {
    this.comparator = comparator;
  }

  public double getThreshold() {
    return threshold;
  }

  public void setThreshold(double threshold) {
    this.threshold = threshold;
  }

  public String getPayer() {
    return payer;
  }

  public void setPayer(String payer) {
    this.payer = payer;
  }

  public LocalDateTime getBeginDate() {
    return this.beginDate;
  }

  public void setBeginDate(String beginDate) {
    this.beginDate = this.ledgerService.getDateFromFormattedString(beginDate);
  }

  public void setBeginDate(LocalDateTime beginDate) {
    this.beginDate = beginDate;
  }

  public LocalDateTime getEndDate() {
    return this.endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = this.ledgerService.getDateFromFormattedString(endDate);
  }

  public void setEndDate(LocalDateTime endDate) {
    this.endDate = endDate;
  }

  public String filter() {
    Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    this.setComparator(requestParams.get("comparator"));
    this.setBeginDate(requestParams.get("beginDate"));
    this.setEndDate(requestParams.get("endDate"));
    this.filterEntries();
    return null;
  }

  public void filterEntries() {
    List<Entry> entries = ledgerService.getFilteredEntries(pageState.getPage().getId(), this.title, this.payer, this.threshold, this.comparator, this.beginDate, this.endDate);
    this.pageState.setEntries(entries);
    this.summaryController.setDirty(true);
  }

  public String clear() {
    this.title = null;
    this.payer = null;
    this.comparator = null;
    this.threshold = 0.00;
    this.beginDate = null;
    this.endDate = null;
    this.pageState.setEntries(null);
    this.summaryController.setDirty(true);
    return null;
  }
}

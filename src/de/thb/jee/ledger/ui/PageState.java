package de.thb.jee.ledger.ui;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import de.thb.jee.ledger.models.Entry;
import de.thb.jee.ledger.models.Page;

@Named
@SessionScoped
public class PageState implements Serializable {

  private static final long serialVersionUID = -338440659060734054L;

  private Page page;
  private List<Entry> entries;

  public List<Entry> getEntries() {
    return entries;
  }

  public void setEntries(List<Entry> entries) {
    this.entries = entries;
  }

  public Page getPage() {
    return page;
  }

  public void setPage(Page page) {
    this.page = page;
  }
}

package de.thb.jee.ledger.ui;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.thb.jee.ledger.logic.LedgerService;
import de.thb.jee.ledger.models.Entry;

@Named
@RequestScoped
public class EntryController implements Serializable {

  private static final long serialVersionUID = 623556647812397099L;
  private Entry entry;
  //propertyActionListener gets called after all setters so we need a copy of the oldEntry
  private Entry oldEntry;
  private Double saldo;

  @Inject
  private LedgerService ledgerService;
  @Inject
  private PageState pageState;

  public EntryController() {
    this.reset();
  }

  public void reset() {
    this.entry = new Entry();
    this.oldEntry = new Entry();
    this.saldo = null;
  }

  public Entry getEntry() {
    return this.entry;
  }

  public void setEntry(Entry entry) {
    this.entry = entry;
  }

  public void setOldEntry(Entry entry) {
    this.oldEntry = entry;
  }

  public String getTitle() {
    return this.entry.getTitle();
  }

  public void setTitle(String title) {
    this.entry.setTitle(title);
  }

  public double getValue() {
    return this.entry.getValue();
  }

  public void setValue(double value) {
    this.entry.setValue(value);
  }

  public LocalDateTime getDate() {
    return this.entry.getDate();
  }

  public void setDate(LocalDateTime date) {
    this.entry.setDate(date);
  }

  public String getPayer() {
    return this.entry.getPayer();
  }

  public void setPayer(String payer) {
    this.entry.setPayer(payer);
  }

  public char getType() {
    if (this.entry.getValue() <= 0) {
      return '-';
    } else {
      return '+';
    }
  }

  public void setType(String type) {
    double oldValue = this.entry.getValue();
    double value = oldValue;
    switch(type) {
      case "-": value = (oldValue < 0) ? oldValue : -oldValue; break;
      case "+": value = (oldValue < 0) ? -oldValue : oldValue; break;
    }

    this.entry.setValue(value);
  }

  public String delete() {
    ledgerService.deleteEntry(oldEntry);
    this.pageState.setEntries(null);
    return null;
  }

  public String save() {
    Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    this.setType(requestParams.get("type"));
    this.setFormattedDate(requestParams.get("date"));
    if(this.oldEntry != null) {
      this.entry.setId(this.oldEntry.getId());
    }
    if(this.entry.getPage() == null) {
      this.entry.setPage(this.pageState.getPage());
    }
    this.ledgerService.createEntry(this.entry);
    this.pageState.setEntries(null);
    this.reset();
    return null;
  }

  public int getDay() {
    return this.entry.getDate().getDayOfMonth();
  }

  public int getMonth() {
    return this.entry.getDate().getMonth().getValue();
  }

  public String getMonthTwoDigits() {
    return String.format("%02d", this.getMonth());
  }

  public String getMonthShort() {
    return this.entry.getDate().getMonth().getDisplayName(TextStyle.SHORT, FacesContext.getCurrentInstance().getViewRoot().getLocale());
  }

  public int getYear() {
    return this.entry.getDate().getYear();
  }

  public String getFormattedDate() {
    return this.ledgerService.getFormattedDateString(this.entry.getDate());
  }

  public void setFormattedDate(String formattedDate) {
    this.entry.setDate(this.ledgerService.getDateFromFormattedString(formattedDate));
  }

  public Double getSaldo() {
    if(this.saldo == null) {
      this.saldo=ledgerService.getSaldo(this.entry.getPage().getId(), this.entry.getDate(), this.entry.getId());
    }
    return this.saldo;
  }
}

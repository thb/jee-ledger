package de.thb.jee.ledger.utilities;

public class MathUtils {
  public static double getPercentage(double value, double total, int fraction) {
    return MathUtils.roundToFraction(value/total*100, fraction);
  }

  public static double roundToFraction(double value, int fraction) {
    return (double) Math.round(value * fraction) / fraction;
  }
}

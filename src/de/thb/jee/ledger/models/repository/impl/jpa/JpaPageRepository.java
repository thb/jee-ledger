package de.thb.jee.ledger.models.repository.impl.jpa;

import javax.enterprise.inject.Default;

import de.thb.jee.ledger.models.Page;
import de.thb.jee.ledger.models.generic.repository.impl.jpa.JpaRepository;
import de.thb.jee.ledger.models.repository.api.PageRepository;

@Default
public class JpaPageRepository extends JpaRepository<Page, Long> implements PageRepository {
}

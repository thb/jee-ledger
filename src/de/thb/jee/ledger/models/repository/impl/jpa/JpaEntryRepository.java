package de.thb.jee.ledger.models.repository.impl.jpa;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.persistence.TypedQuery;

import de.thb.jee.ledger.models.Entry;
import de.thb.jee.ledger.models.generic.repository.impl.jpa.JpaRepository;
import de.thb.jee.ledger.models.repository.api.EntryOrdering;
import de.thb.jee.ledger.models.repository.api.EntryRepository;

@Default
public class JpaEntryRepository extends JpaRepository<Entry, Long> implements EntryRepository {

  @Override
  public Double getSaldo(Long pageId, LocalDateTime date, Long entryId) {
    String queryString = "SELECT SUM(e.value) FROM Entry e WHERE e.page.id = :pageId AND ";
    if (entryId == null) {
      queryString += "e.date <= :date";
    } else {
      queryString += "(e.date < :date OR e.date = :date and e.id <= :entryId)";
    }

    TypedQuery<Double> query = mEm.createQuery(queryString, Double.class);
    query.setParameter("pageId", pageId);
    query.setParameter("date", date);
    if (entryId != null) {
      query.setParameter("entryId", entryId);
    }
    return query.getSingleResult();
  }

  @Override
  public List<Entry> getEntriesOfMonthRange(Long pageId, YearMonth startMonth, YearMonth endMonth) {
    TypedQuery<Entry> query = mEm.createQuery(
        "SELECT e FROM Entry e "+
        "WHERE e.page.id = :pageId "+
          "AND YEAR(e.date) >= :startYear "+
          "AND YEAR(e.date) <= :endYear "+
          "AND MONTH(e.date) >= :startMonth "+
          "AND MONTH(e.date) <= :endMonth "+
        "ORDER BY e.date ASC",
        Entry.class);
    query.setParameter("pageId", pageId);
    query.setParameter("startYear", startMonth.getYear());
    query.setParameter("endYear", endMonth.getYear());
    query.setParameter("startMonth", startMonth.getMonthValue());
    query.setParameter("endMonth", endMonth.getMonthValue());
    return query.getResultList();
  }

  @Override
  public List<Entry> getEntries(Long pageId) {
    return this.getEntries(pageId, JpaEntryRepository.DEFAULT_ORDER);
  }

  @Override
  public List<Entry> getEntries(Long pageId, EntryOrdering ordering) {
    String queryString  = "SELECT e FROM Entry e WHERE e.page.id = :pageId";
    queryString += JpaEntryRepository.getOrderQueryString(ordering);
    TypedQuery<Entry> query = mEm.createQuery(queryString, Entry.class);
    query.setParameter("pageId", pageId);
    return query.getResultList();
  }

  @Override
  public List<Entry> getFilteredEntries(Long pageId, String title, String payer, Double value, String comparator, LocalDateTime beginDate, LocalDateTime endDate) {
    return this.getFilteredEntries(pageId, title, payer, value, comparator, beginDate, endDate, JpaEntryRepository.DEFAULT_ORDER);
  }

  @Override
  public List<Entry> getFilteredEntries(Long pageId, String title, String payer, Double value, String comparator, LocalDateTime beginDate, LocalDateTime endDate, EntryOrdering ordering) {
    String queryString = "SELECT e FROM Entry e WHERE e.page.id = :pageId";
    if (title != null && !title.isEmpty()) {
      queryString += " AND e.title LIKE :title";
    }
    if (payer != null && !payer.isEmpty()) {
      queryString += " AND e.payer LIKE :payer";
    }
    if (comparator != null && !comparator.isEmpty()
        && (comparator.equals("<=") || comparator.equals("=") || comparator.equals(">="))) {
      queryString += " AND e.value " + comparator + " :value";
    }
    if (beginDate != null) {
      queryString += " AND e.date >= :beginDate";
    }
    if (endDate != null) {
      queryString += " AND e.date <= :endDate";
    }
    queryString += JpaEntryRepository.getOrderQueryString(ordering);

    TypedQuery<Entry> query = mEm.createQuery(queryString, Entry.class);

    query.setParameter("pageId", pageId);
    if (title != null && !title.isEmpty()) {
      query.setParameter("title", '%' + title + '%');
    }
    if (payer != null && !payer.isEmpty()) {
      query.setParameter("payer", '%' + payer + '%');
    }
    if (comparator != null && !comparator.isEmpty()
        && (comparator.equals("<=") || comparator.equals("=") || comparator.equals(">="))) {
      query.setParameter("value", value);
    }
    if (beginDate != null) {
      query.setParameter("beginDate", beginDate);
    }
    if (endDate != null) {
      query.setParameter("endDate", endDate);
    }

    return query.getResultList();
  }

  private static String getOrderQueryString(EntryOrdering ordering) {
    String defaultOrder = " ORDER BY e.date DESC, e.id DESC";
    switch(ordering) {
      case ASC:
        return defaultOrder.replace("DESC", "ASC");
      case DESC:
        return defaultOrder;
    }
    return null;
  }
}

package de.thb.jee.ledger.models.repository.api;

public enum EntryOrdering {
  ASC, DESC;
}

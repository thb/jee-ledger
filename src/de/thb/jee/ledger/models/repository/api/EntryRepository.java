package de.thb.jee.ledger.models.repository.api;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.List;

import de.thb.jee.ledger.models.Entry;
import de.thb.jee.ledger.models.generic.repository.api.GenericRepository;

public interface EntryRepository extends GenericRepository<Entry, Long> {
  public static final EntryOrdering DEFAULT_ORDER = EntryOrdering.DESC;

  public Double getSaldo(Long pageId, LocalDateTime date, Long entryId);
  public List<Entry> getEntriesOfMonthRange(Long pageId, YearMonth startMonth, YearMonth endMonth);
  public List<Entry> getEntries(Long pageId);
  public List<Entry> getEntries(Long pageId, EntryOrdering ordering);
  public List<Entry> getFilteredEntries(Long pageId, String title, String payer, Double value, String comparator, LocalDateTime beginDate, LocalDateTime endDate);
  public List<Entry> getFilteredEntries(Long pageId, String title, String payer, Double value, String comparator, LocalDateTime beginDate, LocalDateTime endDate, EntryOrdering ordering);
}

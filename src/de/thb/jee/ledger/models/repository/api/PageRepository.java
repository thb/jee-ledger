package de.thb.jee.ledger.models.repository.api;

import de.thb.jee.ledger.models.Page;
import de.thb.jee.ledger.models.generic.repository.api.GenericRepository;

public interface PageRepository extends GenericRepository<Page, Long>{
}
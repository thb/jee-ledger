package de.thb.jee.ledger.models.generic.repository.impl.jpa;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import de.thb.jee.ledger.models.generic.data.GenericEntity;
import de.thb.jee.ledger.models.generic.repository.api.GenericRepository;

//Copied over from example project
@Transactional(value = TxType.MANDATORY)
public abstract class JpaRepository<EntityType extends GenericEntity<IdType>, IdType>
		implements GenericRepository<EntityType, IdType> {

	// https://developer.jboss.org/wiki/GenericDataAccessObjects
	protected final Class<EntityType> mEntityType;

	protected JpaRepository() {

		// http://blog.xebia.com/acessing-generic-types-at-runtime-in-java/
		Class<?> cls = getClass();
		while (!(cls.getSuperclass() == null || cls.getSuperclass().equals(JpaRepository.class))) {
			cls = cls.getSuperclass();
		}

		if (cls.getSuperclass() == null) {
			throw new RuntimeException("Unexpected exception occurred.");
		}

		this.mEntityType = ((Class) ((ParameterizedType) cls.getGenericSuperclass()).getActualTypeArguments()[0]);
	}

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	protected EntityManager mEm;

	@Override
	public EntityType create(EntityType anEntity) {
		mEm.persist(anEntity);
		return anEntity;
	}

	@Override
	public EntityType read(IdType anId) {
		return mEm.find(mEntityType, anId);
	}

	@Override
	public void update(EntityType anEntity) {
		mEm.merge(anEntity);
	}

	@Override
	public void delete(EntityType anEntity) {
    // we need to ensure, that the EntityManager knows the entity
    mEm.remove(mEm.contains(anEntity) ? anEntity : mEm.merge(anEntity));
	}

	@Override
	public List<EntityType> readAll() {
		return mEm.createQuery("SELECT e FROM " + mEntityType.getName() + " e", mEntityType).getResultList();
	}

}

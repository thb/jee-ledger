package de.thb.jee.ledger.models.generic.repository.api;

import java.util.List;

import de.thb.jee.ledger.models.generic.data.GenericEntity;

//Copied over from example project
public interface GenericRepository<EntityType extends GenericEntity<IdType>, IdType> {

	public EntityType create(EntityType entity);

	public EntityType read(IdType id);

	public void update(EntityType entity);

	public void delete(EntityType entity);

	public List<EntityType> readAll();

}
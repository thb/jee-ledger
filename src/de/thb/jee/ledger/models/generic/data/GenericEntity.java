package de.thb.jee.ledger.models.generic.data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

//Copied over from example project
@MappedSuperclass
public abstract class GenericEntity<IdType> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private IdType id;

	public IdType getId() {
		return id;
	}

	public void setId(IdType anId) {
		id = anId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GenericEntity<?> other = (GenericEntity<?>) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}

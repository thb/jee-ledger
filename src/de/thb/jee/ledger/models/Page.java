package de.thb.jee.ledger.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import de.thb.jee.ledger.models.generic.data.GenericEntity;

@Entity
public class Page extends GenericEntity<Long> {

	private String title;
	@OneToMany(mappedBy="page")
	private List<Entry> entries;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String value) {
		this.title = value;
	}

	public List<Entry> getEntries() {
		return this.entries;
	}

	public void setEntries(List<Entry> value) {
		this.entries = value;
	}
}

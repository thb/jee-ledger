package de.thb.jee.ledger.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import de.thb.jee.ledger.models.generic.data.GenericEntity;

@Entity
public class Entry extends GenericEntity<Long> {

  private String title;
  private double value;
  private String payer;
  private LocalDateTime date;
  @ManyToOne
  @JoinColumn(name="pageId")
  private Page page;

  public Entry() {
    date = LocalDateTime.now();
  }

  public Page getPage() {
    return page;
  }

  public void setPage(Page page) {
    this.page = page;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public double getValue() {
    return value;
  }

  public void setValue(double value) {
    this.value = value;
  }

  public String getPayer() {
    return payer;
  }

  public void setPayer(String author) {
    this.payer = author;
  }

  public LocalDateTime getDate() {
    return this.date;
  }

  public void setDate(LocalDateTime value) {
    this.date = value;
  }
}

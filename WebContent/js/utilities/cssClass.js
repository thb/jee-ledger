bindSetCssClass = function(trigger, container, setBool, cssClass) {
  setBool = (typeof setBool != 'undefined') ? setBool : true;
  cssClass = cssClass || 'open';

  $(document).on('click', trigger, function() {
    if (setBool) {
      return $(container).addClass(cssClass);
    } else {
      return $(container).removeClass(cssClass);
    }
  });
}

toggleCssClass = function(event) {
  baseContainer = event.data.baseContainer;
  baseClass = event.data.baseClass;
  useContainer = event.data.useContainer;
  useClass = event.data.useClass;
  on = event.data.on;

  if((on && baseContainer.hasClass(baseClass)) || (!on && !(baseContainer.hasClass(baseClass)))) {
    useContainer.removeClass(useClass);
  } else {
    useContainer.addClass(useClass);
  }
}

bindToggleCssClass = function(trigger, basedOn, useOn, baseClass, useClass, on) {
  useOn = useOn || basedOn;
  baseClass = baseClass || 'open';
  useClass = useClass || baseClass;
  on = (typeof on != 'undefined') ? on : true;

  baseContainer = $(basedOn);
  useContainer = $(useOn);

  $(document).on('click', trigger,
    {
      baseContainer: baseContainer,
      baseClass: baseClass,
      useContainer: useContainer,
      useClass: useClass,
      on: on
    },
    toggleCssClass
  );
}

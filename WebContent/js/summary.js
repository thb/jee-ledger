var graphOptions = {
  showArea: true,
  lineSmooth: Chartist.Interpolation.none(),
  axisX: {
    type: Chartist.FixedScaleAxis,
    divisor: 5,
    labelInterpolationFnc: function(value) {
      return moment(value).format('MMM D');
    }
  },
  plugins: [
    Chartist.plugins.ctThreshold(),
    Chartist.plugins.tooltip({
      appendToBody: true,
      transformTooltipTextFnc: (value) => { return value.split(',')[1]; }
    })
  ]
};

var bilanceGraph = new Chartist.Line('.bilanceGraph', {
  series: [
    {
      name: "total",
      data: totalGraphData
    },
    {
      name: "selection",
      data: monthGraphData
    }
  ]
}, graphOptions);

var bilancePie = new Chartist.Pie('.bilancePie', {
  series: bilancePieData.values
}, {
  donut: true,
  startAngle: 270,
  total: 200,
  labelInterpolationFnc: function(rawValue, index) { return bilancePieData.labels[index]; }
});

$(document).ready(function() {
  $('.dropdown-toggle').dropdown();
  $('.dropdown-item').on('click', function() {
    $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').html($(this).html());
  });
});

$(document).ready(function() {
  bindToggleCssClass('#light-switch', 'body', 'body', 't-dark', 't-dark');

  bindSetCssClass('.create-item-trigger', '.create-overlay');
  bindSetCssClass('.create-overlay-background,.create-overlay-close', '.create-overlay', false);

  bindSetCssClass('#settings-trigger', '#settings-overlay');
  bindSetCssClass('#settings-overlay-background,.settings-overlay-close', '#settings-overlay', false);

  $(document).on('click', '.filter-trigger', function() {
    var container = $('#filter-form\\:filter-container');
    container.toggleClass('open');
    container.find('.datepicker-container').datepicker({
      todayHighlight: true,
      autoclose: true
    });
    if (container.hasClass('open')) {
      $('.filter-trigger').addClass('up');
    } else {
      $('.filter-trigger').removeClass('up');
    }
  })

  $(document).on('click', '.item-trigger', function() {
    return $(this).parent().find('.item-overlay').addClass('open');
  });

  $(document).on('click', '.item-overlay-background,.item-overlay-close', function() {
    $(this).closest('.item-overlay').removeClass('open');
  });
});

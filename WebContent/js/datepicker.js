$(document).ready(function() {
  $.fn.datepicker.Constructor.prototype.hide = function() {
    let overlay = this.element.closest('.overlay');
    overlay.find('.overlay-container-left').removeClass('side-by-side');
    overlay.find('.overlay-container-right').removeClass('side-by-side');
  }

  $(document).on('changeDate', '.datepicker-container', function() {
      $(this).children('.form-control').val($(this).datepicker('getFormattedDate'));
      let overlay = $(this).closest('.overlay');
      let date = moment($(this).datepicker('getDate'));
      let month = date.format('MMM');
      let day = date.format('DD');
      overlay.find('.date-button.month').text(month);
      overlay.find('.date-button.day').text(day);
  });

  $(document).on('click', '.date-container', function() {
    let overlay = $(this).closest('.overlay');
    overlay.find('.overlay-container-left').toggleClass('side-by-side');
    overlay.find('.overlay-container-right').toggleClass('side-by-side');
    $('.datepicker-container').datepicker({
      todayHighlight: true,
      autoclose: true
    });
  });
});

# Sample Ledger App

## What it is
This is a sample application to demonstrate the learnings of the JEE seminar at the THB.
Because of that it's based on JEE and JSF.

## How to install

### What you need
* Eclipse for JEE Development - https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygen3a
* Wildfly - http://wildfly.org/downloads

the following is just convenience (gets pulled through maven if that is used)
* Node.js - https://nodejs.org/en/download
* NPM/Yarn - https://yarnpkg.com/lang/en/docs/install

### First startup
#### Database configuration
* get a copy of the default configuration from the project snippets (only for project members) if you haven't got one from somewhere else
#### Wildfly configuration
* `-Duser.language=en` in VM-arguments is necessary (in Eclipse double click the server and click `open launch configuration`), because the project has no i18n and need an english server
#### With maven
* `mvn install` or in eclipse `Run as Maven install`
#### With nodejs (convenience)
* `yarn` - to pull the necessary dependencies for the scss transpilation
* `yarn sass:build` - if you don't plan to change scss files

* refresh the project in eclipse (to update the "new" css files)
* setup your wildfly server and start it up

## Development
* if you are changing scss files you have to transpile them to css
* everthing else can get accomplished with eclipse or whatever software you use for editing and building
### With Maven
* `mvn install` or in eclipse `Run as Maven install` (everytime you change smth in the scss files (as eclipse isn't executing the goals when deploying on wildfly))
### With nodejs (convenience)
* `yarn dev` - if you plan to change scss files (it starts a watcher, so everthings gets updated)

### Contributing
Have a look at the issues and pick one (add yourself as the assignee)
If there is none left, open new ones if there is still something to implement
